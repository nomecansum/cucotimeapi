<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/sistemas.php';
 
$database = new Database();
$db = $database->getConnection();
 
 // prepare product object
$product = new Product($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

//echo("Peticion:");
//print_r(file_get_contents("php://input"));
//exit();

 
// set product property values
$tipocarga= $data->tipo_carga;
//$sistema = $data->sistema;
//$accion = $data->accion;
//$update = $data->update;
if($tipocarga=='T'){//Carga total, quiere decir que tenemos que limpiar antes de procesar el json
  	$product->deleteALL();
}

$sis_error=0;
$correcto=true;
 
 $respuesta=array();
 $respuesta["respuesta"]="ok";
 $respuesta["message"]="Proceso realizado correctamente. ". count($data->sistemas) . " Sistemas procesados";
 $respuesta["detalle"]=array();
//Ahora vamos a cargar todos los sistemas, hay que recorrerse el array
foreach($data->sistemas as $item) { //foreach element in $arr
	$product->cod_sistema = $item->sistema;
	$product->accion = $item->accion;
	$product->update = $item->update;
	$product->nombre = $item->nombre;
	$product->distribuidor = $item->distribuidor;
	$product->pais = $item->pais;
	$product->max_version=isset($item->max_version) ? $item->max_version : "";
	if ($tipocarga=='T' || $item->accion=='A')
	{
		
		if($product->create())
		{$product->result="ok";}
		else {
			$respuesta_item = array(
			    "cod_sistema" =>  $product->cod_sistema,
			    "result" => $product->result,
			    "mens_error" => $product->mens_error
				);
			array_push($respuesta["detalle"], $respuesta_item);
		}

	}

	if ($tipocarga=='I' && $item->accion=='B')
	{
		
		if($product->delete())
		{$product->result="ok";}
		else {
			$respuesta_item = array(
			    "cod_sistema" =>  $product->cod_sistema,
			    "result" => $product->result,
			    "mens_error" => $product->mens_error
				);
			array_push($respuesta["detalle"], $respuesta_item);


		}

	}

	if ($tipocarga=='I' && $item->accion=='M')
	{
		
		if($product->update())
		{$product->result="ok";} 
		else {
			$respuesta_item = array(
			    "cod_sistema" =>  $product->cod_sistema,
			    "result" => $product->result,
			    "mens_error" => $product->mens_error
				);
			array_push($respuesta["detalle"], $respuesta_item);
		}

	}
}
if (count($respuesta["detalle"])==0)
{

	unset($respuesta["detalle"]);
} else
	{
		$respuesta["respuesta"]="error";
		$respuesta["message"]="Ocurrieron problemas durante el proceso en " . count($data->sistemas) . " Sistemas. Vea el detalle";
	}
echo json_encode($respuesta);

?>