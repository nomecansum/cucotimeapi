<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/licencias.php';
 
$database = new Database();
$db = $database->getConnection();
 
$product = new Product($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

//echo("Peticion:");
//print_r(file_get_contents("php://input"));
//exit();


 
// set product property values
$product->num_licencia = $data->num_licencia;
$product->cod_sistema = $data->cod_sistema;
$product->nom_cliente = $data->nom_cliente;
$product->category_id = $data->category_id;
$product->fec_ini_uso = $data->fec_ini_uso;
$product->num_empleados = $data->num_empleados;
$product->usu_audit = $data->usu_audit;
$product->num_usos =0;
$product->observaciones = $data->observaciones;
$product->val_key = $data->val_key;
$product->val_identificador_pc = "";
$product->txt_licencia = $data->txt_licencia;
$product->mca_venta = $data->venta;
$product->fec_demo = $data->fec_demo;

//print_r($product);
//exit();


// create the product
if($product->create()){
    echo '{';
    	echo '"respuesta": "ok",';
        echo '"mensaje": "Licencia creada correctamente"';
    echo '}';
}
 
// if unable to create the product, tell the user
else{
    echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error creando licencia"';
    echo '}';
}
?>