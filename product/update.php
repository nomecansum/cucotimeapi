<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/licencias.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set product property values
$product->cod_licencia = $data->cod_licencia;
$product->num_licencia = $data->num_licencia;
$product->cod_sistema = $data->cod_sistema;
$product->nom_cliente = $data->nom_cliente;
$product->observaciones = $data->observaciones;
$product->category_id = $data->category_id;
$product->usu_audit = $data->usu_audit;
$product->txt_licencia = $data->txt_licencia;
$product->mca_venta = $data->mca_venta;
$product->estado = $data->estado;

// update the product
if($product->update()){
    echo '{';
    	echo '"respuesta": "ok",';
        echo '"des_error": "Licencia actualizada"';
    echo '}';
}
 
// if unable to update the product, tell the user
else{
    echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Ocurrio un error al actualizar la licencia."';
    echo '}';
}
?>