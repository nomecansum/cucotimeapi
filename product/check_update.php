<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/sistemas.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// set ID property of product to be edited
if (isset($_GET['cod_sistema'])) {
	$product->cod_sistema = isset($_GET['cod_sistema']) ? $_GET['cod_sistema'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: cod_sistema no definido"';
    echo '}';
    exit();
}

if (isset($_GET['num_licencia'])) {
    $product->num_licencia = isset($_GET['num_licencia']) ? $_GET['num_licencia'] : die();
} else{
    echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: num_licencia no definido"';
    echo '}';
    exit();
}

if (isset($_GET['version'])) {
    $product->version = isset($_GET['version']) ? $_GET['version'] : die();
} else{
    echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: version no definido"';
    echo '}';
    exit();
}
 
// read the details of product to be edited

if($product->readOne()){
   // create array 
   $product_arr = array(
    "respuesta" => "ok",
    "cod_sistema" =>  $product->cod_sistema,
    "update" => $product->update
	);
   if ($product->update=="B"){
    //Esto habra que descomentarlo cuando lo pongamos en marcha
    $product->block();
    echo '{';
    echo '"respuesta": "ok",';
    echo '"des_error": "Error: Licencia bloqueada",';
    echo '"update": "B"';
    echo '}'; 
    } else
       {
         print_r(json_encode($product_arr));
       }
// make it json format
   
} else {//El sistema no esta en la BDD
    echo '{';
    echo '"respuesta": "ok",';
    echo '"des_error": "Error: cod_sistema no existe",';
    echo '"update": "N"';
    echo '}'; 
}

//Por ahora, hasta que les salga de las pelotas, todo el mundo actualiza

/*echo '{';
    echo '"respuesta": "ok",';
    echo '"des_error": "Por ahora no miramos a nadie",';
    echo '"update": "S"';
    echo '}'; */

?>