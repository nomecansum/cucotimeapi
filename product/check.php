<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/licencias.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// set ID property of product to be edited
if (isset($_GET['cod_sistema'])) {
	$product->cod_sistema = isset($_GET['cod_sistema']) ? $_GET['cod_sistema'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: cod_sistema no definido"';
    echo '}';
    exit();
}

if (isset($_GET['num_licencia'])) {
	$product->num_licencia = isset($_GET['num_licencia']) ? $_GET['num_licencia'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: num_licencia no definido"';
    echo '}';
    exit();
}

if (isset($_GET['val_identificador_pc'])) {
	$product->val_identificador_pc = isset($_GET['val_identificador_pc']) ? $_GET['val_identificador_pc'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: val_identificador_pc no definido"';
    echo '}';
    exit();
}

if (isset($_GET['val_key'])) {
	$product->val_key = isset($_GET['val_key']) ? $_GET['val_key'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: val_key no definido"';
    echo '}';
    exit();
}


if (isset($_GET['val_version_sw'])) {
	$product->val_version_sw = isset($_GET['val_version_sw']) ? $_GET['val_version_sw'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: val_version_sw no definido"';
    echo '}';
    exit();
}

if (isset($_GET['complemento'])) {
    $product->complemento = isset($_GET['complemento']) ? $_GET['complemento'] : die();
} else{
    echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: complemento no definido"';
    echo '}';
    exit();
}

$product->txt_licencia=$product->num_licencia.$product->complemento;

 
// read the details of product to be edited
 
$dt = new DateTime();
if($product->readOne()){
   // create array 
	$product->update_check();
   $product_arr = array(
    "cod_licencia" =>  $product->cod_licencia,
    "num_licencia" => $product->num_licencia,
    "cod_sistema" => $product->cod_sistema,
    "txt_licencia" => $product->txt_licencia,
    "respuesta" => "OK",
    "fec_demo" => $product->fec_demo,
    "tipo" => "existe"
    );

    if ($product->fec_demo==0) //Demo caducada
    {
        echo '{';
            echo '"respuesta": "ERROR",';
            echo '"mensaje": "Su demo ha caducado"';
        echo '}';
    } else {
        // make it json format
        print_r(json_encode($product_arr));
    }

}
 
// if unable to create the product
else{
    	//A ver si esta sin asignar
    	if($product->assignate()){
		   //echo "Vamos a jacer el update";
		   if($product->update_assign()){
			    echo '{';
			    	echo '"respuesta": "ok",';
			        echo '"mensaje": "Licencia actualizada"';
			    echo '}';
			}
		}   
}



 

?>