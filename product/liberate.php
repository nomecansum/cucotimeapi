<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/licencias.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));
 
// get product id
if (isset($_GET['cod_licencia'])) {
	$product->cod_licencia = isset($_GET['cod_licencia']) ? $_GET['cod_licencia'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: cod_licencia no definido"';
    echo '}';
    exit();
}

if (isset($_GET['num_licencia'])) {
	$product->num_licencia = isset($_GET['num_licencia']) ? $_GET['num_licencia'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: num_licencia no definido"';
    echo '}';
    exit();
}

if (isset($_GET['txt_licencia'])) {
	$product->txt_licencia = isset($_GET['txt_licencia']) ? $_GET['txt_licencia'] : die();
} else{
	echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error: txt_licencia no definido"';
    echo '}';
    exit();
}

	if ($product->liberate()){
		echo '{';
			echo '"respuesta": "ok",';
	        echo '"mensaje": "Licencia liberada"';
	    echo '}';
	} else
	{
		echo '{';
			echo '"respuesta": "error",';
	        echo '"des_error": "La licencia no existe o ya esta liberada"';
	    echo '}';	
	}
?>