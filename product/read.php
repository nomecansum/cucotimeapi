<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/licencias.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$product = new Product($db);
// get filter
$filtro=isset($_GET["filtro"]) ? $_GET["filtro"] : "";

// query products
$stmt = $product->read($filtro);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $products_arr=array('respuesta'=>'OK');
    //$data['respuesta']="OK";
    //array_push($products_arr, $data);
    $products_arr["licencias"]=array();
    
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "cod_licencia" => $cod_licencia,
            "num_licencia" => $num_licencia,
            "cod_sistema" => $cod_sistema,
            "nom_cliente" => $nom_cliente,
            "observaciones" => html_entity_decode($observaciones),
            "fec_ini_uso" => $fec_ini_uso,
            "fec_ult_uso" => $fec_ult_uso,
            "val_identificador_pc" => $val_identificador_pc,
            "val_version_sw" => $val_version_sw,
            "txt_licencia" => $txt_licencia,
            "num_usos" => $num_usos,
            "val_key" => $val_key,
            "usu_audit" => $usu_audit,
            "num_empleados" => $num_empleados,
            "txt_licencia" => $txt_licencia,
            "mca_venta" => $mca_venta,
            "fec_demo" => $fec_demo,
            "estado" => $estado,
            "nombre" => $nombre,
            "mca_update" => $mca_update,
            "distribuidor" => $distribuidor,
            "pais" => $pais
        );
 
        array_push($products_arr["licencias"], $product_item);

    }
 
    echo json_encode($products_arr);
}
 
else{
    echo json_encode(
        array("respuesta" => "error",
              "des_error" => "No se han encontrado licencias.")
    );
}
?>