<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/licencias.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set product property values
$product->num_licencia = $data->num_licencia;
$product->cod_sistema = $data->cod_sistema;
$product->val_key = $data->val_key;
$product->val_identificador_pc = $data->val_identificador_pc;
$product->val_version_sw = $data->val_version_sw;

// update the product
if($product->assignate()){
   //echo "Vamos a jacer el update";
   if($product->update_assign()){
	    echo '{';
	    	echo '"respuesta": "ok",';
	        echo '"mensaje": "Licencia actualizada"';
	    echo '}';
	}
}
?>