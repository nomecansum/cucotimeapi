<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
 
// include database and object file
include_once '../config/database.php';
include_once '../objects/licencias.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// get product id
if (isset($_GET['cod_licencia'])) {
	$product->cod_licencia = isset($_GET['cod_licencia']) ? $_GET['cod_licencia'] : die();
} else{
	echo '{';
        echo '"message": "Error: cod_licencia no definido"';
    echo '}';
    exit();
}
 
// delete the product
if($product->delete()){
    echo '{';
    	echo '"respuesta": "ok",';
        echo '"mensaje": "Licencia eliminada"';
    echo '}';
}
 
// if unable to delete the product
else{
    echo '{';
        echo '"respuesta": "error",';
        echo '"des_error": "Error al borrar licencia."';
    echo '}';
}
?>