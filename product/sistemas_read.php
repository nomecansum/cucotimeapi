<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/sistemas.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$product = new Product($db);
 
// query products
$stmt = $product->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $products_arr=array();
    $data['respuesta']="OK";
    array_push($products_arr, $data);
    $products_arr["sistemas"]=array();
    
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "cod_sistema" => $cod_sistema,
            "nombre" => $nombre,
            "mca_update" => $mca_update,
            "max_version" => $max_version,
            "distribuidor" => $distribuidor,
            "pais" => $pais
        );
 
        array_push($products_arr["sistemas"], $product_item);

    }
 
    echo json_encode($products_arr);
}
 
else{
    echo json_encode(
        array("respuesta" => "error",
              "des_error" => "No se han encontrado sistemas.")
    );
}
?>