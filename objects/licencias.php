<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "cug_licencias";
 
    // object properties
    public $cod_licencia;
    public $num_licencia;
    public $cod_sistema;
    public $nom_cliente;
    public $category_id;
    public $fec_ini_uso;
    public $fec_ult_uso;
    public $num_usos;
    public $val_identificador_pc;
    public $val_version_sw;
    public $txt_licencia;
    public $observaciones;
    public $val_key;
    public $num_empleados;
    public $usu_audit;
    public $mca_venta;
    public $fec_demo;
    public $complemento;
    public $estado;
    public $nombre;
    public $mca_update;
    public $distribuidor;
    public $pais;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read($filtro){
 
        // select all query
        $query = "SELECT 
                      `cug_licencias`.`cod_licencia`,
                      `cug_licencias`.`num_licencia`,
                      `cug_licencias`.`cod_sistema`,
                      `cug_licencias`.`nom_cliente`,
                      `cug_licencias`.`category_id`,
                      `cug_licencias`.`fec_ini_uso`,
                      `cug_licencias`.`fec_ult_uso`,
                      `cug_licencias`.`num_usos`,
                      `cug_licencias`.`val_identificador_pc`,
                      `cug_licencias`.`val_version_sw`,
                      `cug_licencias`.`txt_licencia`,
                      `cug_licencias`.`observaciones`,
                      `cug_licencias`.`val_key`,
                      `cug_licencias`.`num_empleados`,
                      `cug_licencias`.`usu_audit`,
                      `cug_licencias`.`mca_venta`,
                      `cug_licencias`.`estado`,
                      date(`cug_licencias`.`fec_demo`) as fec_demo,
                      `cug_sistemas`.`nombre`,
                      `cug_sistemas`.`mca_update`,
                      `cug_sistemas`.`distribuidor`,
                      `cug_sistemas`.`pais`
                   FROM
                      `cug_licencias`
                      LEFT OUTER JOIN `cug_sistemas` ON (`cug_licencias`.`cod_sistema` = `cug_sistemas`.`cod_sistema`)
                WHERE
                    1=1";
        switch($filtro){ 
            case 1: 
                $query .=" AND cug_sistemas.mca_update='S'";
                break; 
            case 2:         
                $query.=" AND cug_sistemas.mca_update='N'";
                break; 
            case 3:         
                $query .=" AND cug_sistemas.mca_update='B'";
                break; 
            case 4:         
                $query .=" AND `cug_sistemas`.`pais`='España'";
                break; 
            case 5:         
                $query .=" AND `cug_sistemas`.`pais`<>'España'";
                break; 
            case 6:         
                $query .=" AND `cug_sistemas`.`distribuidor`<>''";
                break; 
            case 7:         
                $query .=" AND `cug_licencias`.`estado`='A'";
                break; 
            case 8:         
                $query .=" AND `cug_licencias`.`estado`='B'";
                break; 
            case 9:         
                $query .=" AND ((`cug_licencias`.`fec_demo`<>null) OR (`cug_licencias`.`fec_demo`<>''))";
                break; 
            case 10:         
                $query .=" AND `cug_licencias`.`fec_demo`>now()";
                break; 
            }        
        $query .=" ORDER BY `cug_licencias`.`nom_cliente`";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    // create product
    function create(){
 
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    num_licencia=:num_licencia, cod_sistema=:cod_sistema, nom_cliente=:nom_cliente, category_id=:category_id, fec_ini_uso=:fec_ini_uso, num_usos=:num_usos, num_empleados=:num_empleados, usu_audit=:usu_audit, val_key=:val_key, val_identificador_pc=:val_identificador_pc, observaciones=:observaciones,txt_licencia=:txt_licencia,mca_venta=:mca_venta,fec_demo=:fec_demo";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->nom_cliente=htmlspecialchars(strip_tags($this->nom_cliente));
        $this->observaciones=htmlspecialchars(strip_tags($this->observaciones));
     
        // bind values
        $stmt->bindParam(":num_licencia", $this->num_licencia);
        $stmt->bindParam(":cod_sistema", $this->cod_sistema);
        $stmt->bindParam(":nom_cliente", $this->nom_cliente);
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":fec_ini_uso", $this->fec_ini_uso);
        $stmt->bindParam(":num_usos", $this->num_usos);
        $stmt->bindParam(":num_empleados", $this->num_empleados);
        $stmt->bindParam(":usu_audit", $this->usu_audit);
        $stmt->bindParam(":observaciones", $this->observaciones);
        $stmt->bindParam(":val_key", $this->val_key);
        $stmt->bindParam(":val_identificador_pc", $this->val_identificador_pc);
        $stmt->bindParam(":txt_licencia", $this->txt_licencia);
        $stmt->bindParam(":mca_venta", $this->mca_venta);
        $stmt->bindParam(":fec_demo", $this->fec_demo);


        // execute query
        if($stmt->execute()){
            return true;
        }
        
        return false;
    }
    // used when filling up the update product form
    function readOne(){
     
        // query to read single record
        $query = "SELECT 
                      `cug_licencias`.`cod_licencia`,
                      `cug_licencias`.`num_licencia`,
                      `cug_licencias`.`cod_sistema`,
                      `cug_licencias`.`txt_licencia`,
                      if((`cug_licencias`.`fec_demo` is null) OR (`cug_licencias`.`fec_demo`>now()),'1','0') as fec_demo
                    FROM
                      `cug_licencias`
                WHERE
                    cod_sistema= :cod_sistema and
                    num_licencia= :num_licencia and
                    val_identificador_pc= :val_identificador_pc and
                    estado<>'B'
                LIMIT
                    0,1";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
     
       // bind params
        $stmt->bindParam(':cod_sistema', $this->cod_sistema);
        $stmt->bindParam(':num_licencia', $this->num_licencia);
        $stmt->bindParam(':val_identificador_pc', $this->val_identificador_pc);


        // execute query
        $result=$stmt->execute();
        $num = $stmt->rowCount();

        if($num>0){
           // get retrieved row
            $row = $stmt->fetch(PDO::FETCH_ASSOC);     
            // set values to object properties
            $this->cod_licencia = $row['cod_licencia'];
            $this->num_licencia = $row['num_licencia'];
            $this->cod_sistema = $row['cod_sistema'];
            $this->txt_licencia = $row['txt_licencia'];
            $this->fec_demo = $row['fec_demo'];
            return true;
        } else {
            return false;
         }
    }
    // delete the product
    function delete(){
     
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE cod_licencia = ?";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->cod_licencia=htmlspecialchars(strip_tags($this->cod_licencia));
     
        // bind id of record to delete
        $stmt->bindParam(1, $this->cod_licencia);
        $stmt->execute();

        // execute query
       if($stmt->rowCount()>0){
            return true;
        }
        else {
            return false;}
    }
    // update the product
    function update(){
     
        // update query
        $query = "UPDATE
                      cug_licencias
                    SET
                      nom_cliente = :nom_cliente,
                      category_id = :category_id,
                      observaciones = :observaciones,
                      usu_audit = :usu_audit,
                      mca_venta = :mca_venta,
                      fec_demo = :fec_demo,
                      txt_licencia= :txt_licencia,
                      estado=:estado
                    WHERE
                      cod_licencia = :cod_licencia";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->observaciones=htmlspecialchars(strip_tags($this->observaciones));
        $this->nom_cliente=htmlspecialchars(strip_tags($this->nom_cliente));
      
        // bind new values
        $stmt->bindParam(':cod_licencia', $this->cod_licencia);
        $stmt->bindParam(':nom_cliente', $this->nom_cliente);
        $stmt->bindParam(':category_id', $this->category_id);
        $stmt->bindParam(':observaciones', $this->observaciones);
        $stmt->bindParam(":usu_audit", $this->usu_audit);
        $stmt->bindParam(":mca_venta", $this->mca_venta);
        $stmt->bindParam(":fec_demo", $this->fec_demo);
        $stmt->bindParam(":txt_licencia", $this->txt_licencia);
        $stmt->bindParam(":estado", $this->estado);


        if($stmt->execute() && $stmt->rowCount()>0){
            return true;
        }
            else{
                return false;
        }
    }
     // update the product
    function update_check(){
     
        // update query
        $query = "UPDATE
                  cug_licencias
                SET
                  fec_ult_uso = now(),
                  num_usos = num_usos+1,
                  val_version_sw = :val_version_sw,
                  txt_licencia= :txt_licencia
                WHERE
                  cod_licencia = :cod_licencia";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // bind new values
        $stmt->bindParam(':val_version_sw', $this->val_version_sw);
        $stmt->bindParam(':cod_licencia', $this->cod_licencia);
        $txt_licencia = $this->num_licencia.'-'.$this->complemento;
        $stmt->bindParam(':txt_licencia', $txt_licencia);
        // execute the query
        

        if($stmt->execute()){
            return true;
        }
            else{
                return false;
        }
    }
    //cambios despues de la asignacion
    function update_assign(){


        // update query
        $query = "UPDATE
                  cug_licencias
                SET
                  fec_ult_uso = now(),
                  val_version_sw = :val_version_sw,
                  val_identificador_pc= :val_identificador_pc,
                  val_key= :val_key,
                  estado='A'
                WHERE
                  cod_licencia = :cod_licencia";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // bind new values
        $stmt->bindParam(':val_version_sw', $this->val_version_sw);
        $stmt->bindParam(':val_key', $this->val_key);
        $stmt->bindParam(':val_identificador_pc', $this->val_identificador_pc);
        $stmt->bindParam(':cod_licencia', $this->cod_licencia);
        // execute the query
        //print_r($this);
        //$stmt->debugDumpParams();
        //exit();

        if($stmt->execute() && $stmt->rowCount()>0){
            return true;
        }
            else{
                echo '{';
                        echo '"respuesta": "error",';
                        echo '"des_error": "Ocurrio un error al asignar la licencia. up_assign"';
                  echo '}';
                 return false;
        }
    }
    //asignacion de licencia
    function assignate(){
     
        //Primero comprobamos que no esta ya usada
        $query = "SELECT 
                      `cug_licencias`.`cod_licencia`
                    FROM
                      `cug_licencias`
                  WHERE
                  num_licencia = :num_licencia and
                  cod_sistema = :cod_sistema and
                  (val_identificador_pc = '' OR val_identificador_pc is null)";
        $stmt = $this->conn->prepare($query);
     
        // bind new values
        $stmt->bindParam(':num_licencia', $this->num_licencia);
        $stmt->bindParam(':cod_sistema', $this->cod_sistema);

         if($stmt->execute() && $stmt->rowCount()>0){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);     
            // set values to object properties
            $this->cod_licencia = $row['cod_licencia'];
            //echo "OK, podemos asignar";   
            return true;
        }
            else{
                //Vemos a ver si la licencia esta en uso
                $query = "SELECT 
                      `cug_licencias`.`cod_licencia`,
                      `cug_licencias`.`estado`
                    FROM
                      `cug_licencias`
                  WHERE
                  num_licencia = :num_licencia and
                  cod_sistema = :cod_sistema";
                 $stmt = $this->conn->prepare($query);
                 $stmt->bindParam(':num_licencia', $this->num_licencia);
                 $stmt->bindParam(':cod_sistema', $this->cod_sistema);
                 if($stmt->execute() && $stmt->rowCount()>0){
                      echo '{';
                            $row = $stmt->fetch(PDO::FETCH_ASSOC); 
                            if ($row['estado']=='B') { 
                            echo '"respuesta": "error",';
                            echo '"des_error": "Licencia bloqueada"';
                            echo '}';
                          } else {
                            echo '"respuesta": "error",';
                            echo '"des_error": "Licencia en uso."';
                            echo '}';  
                          }
                     return false;
                 } else{
                     echo '{';
                            echo '"respuesta": "error",';
                            echo '"des_error": "Licencia no existe."';
                      echo '}';
                     return false;

                 }               
        }          
    }
    //liberate the product
    function liberate(){
     
        // update query
        $query = "UPDATE
                      cug_licencias
                    SET
                      val_identificador_pc = null,
                      val_key = null,
                      fec_ult_uso=null,
                      num_usos=0,
                      val_version_sw = null,
                      estado=null,
                      txt_licencia= :txt_licencia,
                      num_licencia= :num_licencia
                    WHERE
                      cod_licencia = :cod_licencia";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // bind new values
        $stmt->bindParam(':cod_licencia', $this->cod_licencia);
        $stmt->bindParam(':txt_licencia', $this->txt_licencia);
        $stmt->bindParam(':num_licencia', $this->num_licencia);


        // execute the query
        

        if($stmt->execute()){
            return true;
        }
            else{
                return false;
        }
    }
    // search products
    function search($keywords,$filtro){
 
        // select all query
        $query = "SELECT 
                      `cug_licencias`.`cod_licencia`,
                      `cug_licencias`.`num_licencia`,
                      `cug_licencias`.`cod_sistema`,
                      `cug_licencias`.`nom_cliente`,
                      `cug_licencias`.`category_id`,
                      `cug_licencias`.`fec_ini_uso`,
                      `cug_licencias`.`fec_ult_uso`,
                      `cug_licencias`.`num_usos`,
                      `cug_licencias`.`val_identificador_pc`,
                      `cug_licencias`.`val_version_sw`,
                      `cug_licencias`.`txt_licencia`,
                      `cug_licencias`.`observaciones`,
                      `cug_licencias`.`val_key`,
                      `cug_licencias`.`num_empleados`,
                      `cug_licencias`.`usu_audit`,
                      `cug_licencias`.`mca_venta`,
                      date(`cug_licencias`.`fec_demo`) as fec_demo,
                      `cug_licencias`.`estado`,
                      `cug_sistemas`.`nombre`,
                      `cug_sistemas`.`mca_update`,
                      `cug_sistemas`.`distribuidor`,
                      `cug_sistemas`.`pais`
                   FROM
                      `cug_licencias`
                      LEFT OUTER JOIN `cug_sistemas` ON (`cug_licencias`.`cod_sistema` = `cug_sistemas`.`cod_sistema`)
                WHERE
                    (upper(`cug_licencias`.`num_licencia`) LIKE ? OR upper(`cug_sistemas`.`nombre`) LIKE ? OR upper(`cug_licencias`.`cod_sistema`) LIKE ? OR upper(`cug_licencias`.`txt_licencia`) LIKE ? OR upper(`cug_licencias`.`observaciones`) LIKE ?) ";
                
                switch($filtro){ 
                    case 1: 
                        $query .=" AND cug_sistemas.mca_update='S'";
                        break; 
                    case 2:         
                        $query.=" AND cug_sistemas.mca_update='N'";
                        break; 
                    case 3:         
                        $query .=" AND cug_sistemas.mca_update='B'";
                        break; 
                    case 4:         
                        $query .=" AND `cug_sistemas`.`pais`='España'";
                        break; 
                    case 5:         
                        $query .=" AND `cug_sistemas`.`pais`<>'España'";
                        break; 
                    case 6:         
                        $query .=" AND `cug_sistemas`.`distribuidor`<>''";
                        break; 
                    case 7:         
                        $query .=" AND `cug_licencias`.`estado`='A'";
                        break; 
                    case 8:         
                        $query .=" AND `cug_licencias`.`estado`='B'";
                        break; 
                    case 9:         
                        $query .=" AND ((`cug_licencias`.`fec_demo` is not null))";
                        break; 
                    case 10:         
                        $query .=" AND `cug_licencias`.`fec_demo`>now()";
                        break; 
                    }        
        $query .=" ORDER BY `cug_licencias`.`nom_cliente`";
        //var_dump($query);
        //exit();
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords=strtoupper($keywords);
        $keywords = "%{$keywords}%";
     
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);
        $stmt->bindParam(4, $keywords);
        $stmt->bindParam(5, $keywords);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
}
?>