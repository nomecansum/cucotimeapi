<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "cug_sistemas";
 
    // object properties
    public $cod_sistema;
    public $accion;
    public $update;
    public $nombre;
    public $distribuidor;
    public $pais;
    public $max_version;
    public $rowid;
    public $result;
    public $mens_error;
    public $num_licencia;
  
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read(){
 
        // select all query
        $query = "SELECT 
                      `cug_sistemas`.`cod_sistema`,
                      `cug_sistemas`.`mca_update`,
                      `cug_sistemas`.`max_version`,
                      `cug_sistemas`.`nombre`,
                      `cug_sistemas`.`distribuidor`,
                      `cug_sistemas`.`pais`
                   FROM
                    " . $this->table_name . " 
                ORDER BY
                   cod_sistema";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
    // create product
    function create(){
 
        // query to insert record
        $query = "INSERT INTO
                    cug_sistemas
                SET
                    cod_sistema=:cod_sistema, mca_update=:update, max_version=:max_version, nombre=:nombre, distribuidor=:distribuidor,pais=:pais";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
        // bind values

        $stmt->bindParam(":cod_sistema", $this->cod_sistema);
        $stmt->bindParam(":update", $this->update);
        $stmt->bindParam(":max_version", $this->max_version);
        $stmt->bindParam(":nombre", $this->nombre);
        $stmt->bindParam(":distribuidor", $this->distribuidor);
        $stmt->bindParam(":pais", $this->pais);
        // execute query
        try{
          $stmt->execute();
           $this->result="ok";
            return true;
          } catch(Exception $e){
          $this->result="error";
          $this->mens_error=$e->getMessage();
         return false;
        }
    }
    // used when filling up the update product form
    function readOne(){
     
        // query to read single record
        $query = "SELECT 
                      `cug_sistemas`.`cod_sistema`,
                      `cug_sistemas`.`mca_update`,
                      `cug_sistemas`.`max_version`,
                      `cug_sistemas`.`nombre`,
                      `cug_sistemas`.`distribuidor`,
                      `cug_sistemas`.`pais`
                    FROM
                      `cug_sistemas`
                WHERE
                    cod_sistema= :cod_sistema 
                LIMIT
                    0,1";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
     
       // bind params
        $stmt->bindParam(':cod_sistema', $this->cod_sistema);

        // execute query
        $result=$stmt->execute();
        $num = $stmt->rowCount();
        if($num>0){
           // get retrieved row
            $row = $stmt->fetch(PDO::FETCH_ASSOC);     
            // set values to object properties
            $this->cod_sistema = $row['cod_sistema'];
            $this->update = $row['mca_update'];
            $this->max_version = $row['max_version'];
            $this->nombre = $row['nombre'];
            $this->distribuidor = $row['distribuidor'];
            $this->pais = $row['pais'];
            return true;
        } else {
            return false;
         }
    }
    // delete the product
    function delete(){

        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE cod_sistema = ?";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
          
        // bind id of record to delete
        $stmt->bindParam(1, $this->cod_sistema);
        $stmt->execute();

        // execute query
       if($stmt->rowCount()>0){
            return true;
        }
        else {
            return false;}
    }
    // update the product
    function update(){
     
        // update query
        $query = "UPDATE
                      cug_sistemas
                    SET
                      mca_update = :mca_update,
                      max_version = :max_version,
                      nombre = :nombre,
                      distribuidor=:distribuidor,
                      pais=:pais
                    WHERE
                      cod_sistema = :cod_sistema";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // bind new values
        $stmt->bindParam(':mca_update', $this->update);
        $stmt->bindParam(':max_version', $this->max_version);
        $stmt->bindParam(':cod_sistema', $this->cod_sistema);
        $stmt->bindParam(':nombre', $this->nombre);
        $stmt->bindParam(":distribuidor", $this->nombre);
        $stmt->bindParam(":pais", $this->nombre);

        try{
          $stmt->execute();
           $this->result="ok";
            return true;
          } catch(Exception $e){
          $this->result="error";
          $this->mens_error=$e->getMessage();
         return false;
        }
    }
    //Bloquear una licencia
    function block(){
     
        // update query
        $query = "UPDATE
                      cug_licencias
                    SET
                      estado = 'B'
                    WHERE
                      cod_sistema = :cod_sistema and
                      num_licencia = :num_licencia";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // bind new values
        $stmt->bindParam(':cod_sistema', $this->cod_sistema);
        $stmt->bindParam(':num_licencia', $this->num_licencia);

        try{
          $stmt->execute();
           $this->result="ok";
            return true;
          } catch(Exception $e){
          $this->result="error";
          $this->mens_error=$e->getMessage();
         return false;
        }
    }
     // borrar todos los sistemas the product
    function deleteALL(){
     
        // delete query
        $query = "DELETE FROM " . $this->table_name;
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        $stmt->execute();

        // execute query
       if($stmt->rowCount()>0){
            return true;
        }
        else {
            return false;
        }
    }
  }
?>